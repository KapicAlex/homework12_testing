import CartParser from './CartParser';
import path from 'path';

let parser;

beforeEach(() => {
	parser = new CartParser();
});

describe('CartParser - unit tests', () => {
  describe('readFile', () => {
    it('should return string from input .csv file', () => {
      const csvPath = path.join(__dirname, '../samples/cart.csv');
      const readResult = parser.readFile(csvPath);

      expect(typeof readResult).toBe('string');
    });
  });

	describe('validate', () => {
    it('should return error if headers from contents does not match to schema', () => {
      const errorResult = parser.validate('Product name, Price, Some-Name \n prod1, 10');
      
      expect(errorResult).not.toHaveLength(0);
      expect(errorResult).toBeInstanceOf(Array);
      expect(errorResult[0].type).toBe('header');
    });

    it('should return error if amount of given cells does not match to amount of schema cells', () => {
      const errorResult = parser.validate('Product name, Price, Quantity \n prod1, 10');
      
      expect(errorResult).not.toHaveLength(0);
      expect(errorResult).toBeInstanceOf(Array);
      expect(errorResult[0].type).toBe('row');
    });

    it('should return an error if these cells contain a cell with an empty string instead of a text string', () => {
      const errorResult = parser.validate('Product name, Price, Quantity \n , 10 ,25');
      
      expect(errorResult).not.toHaveLength(0);
      expect(errorResult).toBeInstanceOf(Array);
      expect(errorResult[0].type).toBe('cell');
    });

    it('should return an error if given cells contain a not positive number instead of it', () => {
      const errorResult1 = parser.validate('Product name, Price, Quantity \n prod1, prod2 ,25');
      const errorResult2 = parser.validate('Product name, Price, Quantity \n prod1, -25 , 0');

      expect(errorResult1).not.toHaveLength(0);
      expect(errorResult1).toBeInstanceOf(Array);
      expect(errorResult1[0].type).toBe('cell');

      expect(errorResult2).not.toHaveLength(0);
      expect(errorResult2).toBeInstanceOf(Array);
      expect(errorResult2[0].type).toBe('cell');
    });

    it('should return an empty array if contents (from .csv file) is valid', () => {
      const errorResult = parser.validate('Product name, Price, Quantity \n prod1, 10 ,25');
      
      expect(errorResult).toBeInstanceOf(Array);
      expect(errorResult).toHaveLength(0);
    });
  });

  describe('parceLine', () => {
    it('should correctly parse line (from .csv file)', () => {
      const parseLineResult = parser.parseLine('prod1, 10 ,25');
      const isId = parseLineResult.hasOwnProperty('id');
      const mockParseLineResult = {
        'name': 'prod1',
        'price': 10,
        'quantity': 25
      };

      expect(isId).toBeTruthy();
      expect(parseLineResult.id).toMatch(/^[0-9A-F]{8}-[0-9A-F]{4}-[4][0-9A-F]{3}-[89AB][0-9A-F]{3}-[0-9A-F]{12}$/i);
      delete parseLineResult.id;
      expect(parseLineResult).toEqual(mockParseLineResult);
    });
  });

  describe('calcTotal', () => {
    it('should correctly calculate total items prise', () => {
      const mock1 = [
        { name: 'prod1', price: 5, quantity: 5 }
      ];
      const mock2 = [
        { name: 'prod1', price: 3, quantity: 2 },
        { name: 'prod2', price: 7, quantity: 1 }
      ];
      const mock3 = [
        { name: 'prod1', price: 1.9999, quantity: 5 },
        { name: 'prod2', price: 10.333, quantity: 0 },
        { name: 'prod3', price: 3.3333, quantity: 3 },
      ];

      const calcResult0 = parser.calcTotal([]);
      const calcResult1 = parser.calcTotal(mock1);
      const calcResult2 = parser.calcTotal(mock2);
      const calcResult3 = parser.calcTotal(mock3);

      expect(calcResult0).toBe(0);
      expect(calcResult1).toBe(25);
      expect(calcResult2).toBe(13);
      expect(calcResult3).toBeCloseTo(20);
    });
  });

  describe('creatError', () => {
    it('should return not undefined value', () => {
      const mockErrorBody = ['type', 0, 0, 'message'];
      const errorResult = parser.createError(...mockErrorBody);

      expect(errorResult).not.toBeUndefined();
    });
  });

  describe('parse', () => {
    it('should throw error if .csv file have incorrect data', () => {
      const badCsv = `Product name,Price,Quantity
				              Mollis consequat,9.00,2
				              Tvoluptatem,**BUG**,1`;

      jest.spyOn(parser, 'readFile').mockImplementation(() => badCsv);
      console.error = jest.fn();
      expect(() => parser.parse()).toThrow('Validation failed!');
    });

    it('should return object if .csv file have correct data', () => {
      const goodCsv = `Product name,Price,Quantity
				              Mollis consequat,9.00,2
				              Tvoluptatem,2,1`;

      jest.spyOn(parser, 'readFile').mockImplementation(() => goodCsv);
      
      expect(parser.parse()).not.toBeInstanceOf(Array);
      expect(parser.parse()).toBeInstanceOf(Object);
    });
  });
});

describe('CartParser - integration test', () => {
  describe('cart.json', () => {
    it('should be equal to parsed .csv file', () => {
        const defaultResult = require('../samples/cart.json');
        const csvPath = path.join(__dirname, '../samples/cart.csv');
        const parseResult = parser.parse(csvPath);

        parseResult.items.forEach(item => delete item.id);
        defaultResult.items.forEach(item => delete item.id);

        expect(parseResult).toEqual(defaultResult);
    });
  });
});